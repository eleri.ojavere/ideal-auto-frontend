import React from 'react';
import AppDataContext from '@optimistdigital/create-frontend/universal-react/AppDataContext';

export default function ClaimsSection(data) {
  const totalWords = data.text.title;

  const otherWords = totalWords.split(' ').slice(3, 5).join(' ');

  const firstWord = totalWords.split(' ').slice(0, 3).join(' ');

  return (
    <div className="section-3">
      <div className="section-3-wrap">
        <div className="section-3-left-wrap">
          <div className="section-3-left">
            <div className="title1">
              {firstWord}
              <span> {otherWords}</span>
            </div>

            <div className="line2"></div>

            <div className="section-3-services">
              {data.text.services.map((item, i) => (
                <div className="services-wrap" key={i}>
                  <img
                    className="btn-icon"
                    src={require('../images/Disain/icons/ideal-auto-icons/checkmark_full.svg')}
                  ></img>
                  {item.attributes.service}
                </div>
              ))}

              <div className="button-wrapper">
                <button className="btn-secondary">
                  <span className="btn-text">{data.text.button_1}</span>
                </button>

                <button className="btn-black">
                  <span className="btn-text">{data.text.button_2}</span>
                </button>
              </div>

              <div className="services-wrap">
                <a href="#">
                  <img className="btn-icon" src={require('../images/Disain/icons/ideal-auto-icons/chevron.svg')}></img>
                  <div>{data.text.additional_actions}</div>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div className="section-3-right">
          <img src={data.text.background_image}></img>
          <img className="honeycomb" src={require('../images/Disain/honeycombs/honeycomb.svg')} alt=""></img>
        </div>
      </div>
    </div>
  );
}
