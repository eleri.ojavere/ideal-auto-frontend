import React from 'react';
import AppDataContext from '@optimistdigital/create-frontend/universal-react/AppDataContext';

export default function UsedCarsSaleInfo(data) {
  return (
    <div className="contactinfo-section-container">
      <div className="image-wrapper">
        <div className="image-wrap"></div>
      </div>
      <div className="section-wrapper">
        <div className="section-content">
          <div className="title">{data.text.title}</div>

          <div className="line2"></div>
          <div className="contact-info-wrapper">
            {data.text.promises.map((item, i) => (
              <div className="services-wrap" key={i}>
                <img className="btn-icon" src={require('../images/Disain/icons/ideal-auto-icons/checkmark.svg')}></img>
                <div className="text">{item.attributes.promise}</div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
