import React from 'react';
import AppDataContext from '@optimistdigital/create-frontend/universal-react/AppDataContext';
import Chevron from '../images/Disain/icons/ideal-auto-icons/chevron.svg';

export default function FirstSection(data) {
  return (
    <section className={'section-1'}>
      <div className="honeycomb-wrapper">
        <img className="honeycomb-section-1" src={require('../images/Disain/honeycombs/honeycomb_MERGED.svg')}></img>
      </div>
      <div className="background">
        <img className="background" src={data.text.thumbnail} alt="Services section image"></img>
      </div>
      <div className="total-1-section">
        <div className="section-1-wrapper">
          <h1 className="title1">
            <span className="green-text"></span>
            {data.text.slogan}
          </h1>
          <div className="line"></div>
          <div className="title2">
            <p>{data.text.title}</p>
          </div>
          <div className="services-wrapper">
            {data.text.services.map((item, i) => (
              <button className="btn-primary" key={i}>
                <span className="btn-text"> {item.attributes.service}</span>
                <img className="btn-icon" src={Chevron}></img>
              </button>
            ))}
          </div>
        </div>
      </div>

      <div className="full-footer">
        <div className="footer-1-wrapper">
          <div className="footer-1">
            <div className="footer-item item1">
              <img className="footer-icon" src={require('../images/Disain/icons/ideal-auto-icons/time.svg')}></img>

              <div className="footer-text">
                <p>E-R 8-17 / AUTOMÜÜK 9-18, L 10-14 / PESUTÄNAV 24-7</p>
              </div>
            </div>

            <div className="footer-item item2">
              <img className="footer-icon" src={require('../images/Disain/icons/ideal-auto-icons/location.svg')}></img>
              <div className="footer-text">
                <p>PETERBURI TEE 47/1, TALLINN 11415</p>
              </div>
            </div>

            <div className="footer-item item3">
              <img className="footer-icon" src={require('../images/Disain/icons/ideal-auto-icons/phone.svg')}></img>
              <div className="footer-text">
                <p>+372 663 0873</p>
              </div>
            </div>

            <div className="footer-item item4">
              <img className="footer-icon" src={require('../images/Disain/icons/ideal-auto-icons/facebook.svg')}></img>
              <div className="footer-text">
                <p>FACEBOOK</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
