import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import { AppDataContext } from '@optimistdigital/create-frontend/universal-react';
import _ from 'lodash';

export default function Header() {
  const {
    pageData: { locales, content },
  } = React.useContext(AppDataContext);

  const [isMobileMenuOpen, setMobileMenuOpen] = useState(false);
  const [isMobileMenuItemsOpen, setMobileMenuItemsOpen] = useState(false);

  const toggleMobileMenu = (nextState) => {
    if (isMobileMenuOpen === nextState) {
      return;
    }

    if (!isMobileMenuOpen) {
      setMobileMenuOpen(true);
      const mainElement = document.querySelector('body');
      mainElement.classList.add('mobile-menu-active');
      mainElement.classList.add('burger-menu-overlay');
    } else {
      setMobileMenuOpen(false);
      const mainElement = document.querySelector('body');
      mainElement.classList.remove('mobile-menu-active');
      mainElement.classList.remove('burger-menu-overlay');
    }
  };

  const toggleMobileMenuItems = (nextState) => {
    if (isMobileMenuItemsOpen === nextState) {
      return;
    }

    if (!isMobileMenuItemsOpen) {
      setMobileMenuItemsOpen(true);
      const mainElement = document.querySelector('html');
      mainElement.classList.add('mobile-menu-items-active');
    } else {
      setMobileMenuItemsOpen(false);
      const mainElement = document.querySelector('html');
      mainElement.classList.remove('mobile-menu-items-active');
    }
  };

  return (
    <section>
      <div className="page-header-wrap">
        <div className="header-background-container">
          <div className="svg-background-container">
            <img className="svg-background" src={require('../../images/Disain/icons/header.svg')} />
          </div>
        </div>
        <div className="header-wrapper">
          <NavLink to="/" className="logo" href=""></NavLink>

          <nav className="navbar">
            <ul className="navbar-container">
              <li className="nav-item nav-item-expanded">
                <a className="nav-link expanded" href="#">
                  Teenused{' '}
                  <img
                    className="navbar-icon "
                    src={require('../../images/Disain/icons/ideal-auto-icons/chevron.svg')}
                  ></img>
                </a>
                <ul className="expanded-menu">
                  <li className="nav-item">
                    <NavLink to="/kasutatud-autode-muuk" className="nav-link" onClick={() => toggleMobileMenu(false)}>
                      {' '}
                      Kasutatud autode müük
                    </NavLink>
                  </li>

                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      Automaat ja käsipesu
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      Rehvitöökoda
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      Kere- ja värvitööd
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      Kahjukäsitlus
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      Pane auto müüki
                    </a>
                  </li>
                </ul>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  {' '}
                  Firmast
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  {' '}
                  Nipid
                </a>
              </li>
              <li className="nav-item">
                <NavLink to="/kontakt" className="nav-link" href="#">
                  {' '}
                  Kontakt
                </NavLink>
              </li>
            </ul>
          </nav>
          <div className="language-switcher">
            {Object.entries(locales.locales).map(([localeKey, locale]) => {
              return (
                <LangSwitcherItem
                  locale={locale}
                  localeKey={localeKey}
                  key={localeKey}
                  path={content.localePaths[localeKey]}
                />
              );
            })}
          </div>
          <div className="burger-menu" id="#burger-menu" onClick={() => toggleMobileMenu()}>
            <div className="bar bar-1"></div>
            <div className="bar bar-2"></div>
            <div className="bar bar-3"></div>
          </div>
        </div>

        <nav className={`burger-navbar ${isMobileMenuOpen ? 'is-active' : ''}`}>
          <div className="cross-icon" onClick={() => toggleMobileMenu(false)}>
            <div className="cross cross-1"></div>
            <div className="cross cross-2"></div>
          </div>
          <ul className="burger-navbar-container expanded-container">
            <li className="burger-nav-item">
              <div className="burger-nav-item-expanded">
                <a className="burger-nav-link"> Teenused </a>
                <a className="burger-nav-link link-arrow" onClick={() => toggleMobileMenuItems()}>
                  <img src={require('../../images/Disain/icons/ideal-auto-icons/mobile-arrow.svg')}></img>
                </a>
              </div>
              <ul className={`burger-expanded-menu ${isMobileMenuItemsOpen ? 'open-menu' : ''}`}>
                <li className="nav-item">
                  <NavLink to="/kasutatud-autode-muuk" className="nav-link" onClick={() => toggleMobileMenu(false)}>
                    {' '}
                    Kasutatud autode müük
                  </NavLink>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#" onClick={() => toggleMobileMenu(false)}>
                    Automaat ja käsipesu
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#" onClick={() => toggleMobileMenu(false)}>
                    Rehvitöökoda
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#" onClick={() => toggleMobileMenu(false)}>
                    Kere- ja värvitööd
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#" onClick={() => toggleMobileMenu(false)}>
                    Kahjukäsitlus
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#" onClick={() => toggleMobileMenu(false)}>
                    Pane auto müüki
                  </a>
                </li>
              </ul>
            </li>
          </ul>
          <ul className=" burger-navbar-container">
            <li className="burger-nav-item">
              <a className="burger-nav-link" href="#" onClick={() => toggleMobileMenu(false)}>
                {' '}
                Firmast
              </a>
            </li>
            <li className="burger-nav-item">
              <a className="burger-nav-link" href="#" onClick={() => toggleMobileMenu(false)}>
                {' '}
                Nipid
              </a>
            </li>
            <li className="burger-nav-item">
              <NavLink to="/kontakt" className="burger-nav-link" href="#" onClick={() => toggleMobileMenu(false)}>
                {' '}
                Kontakt
              </NavLink>
            </li>
          </ul>
        </nav>
      </div>
    </section>
  );
}

function LangSwitcherItem({ locale, localeKey, style, path }) {
  return (
    <div className={`lang-item ${style ?? ''}`} id={localeKey}>
      <NavLink exact activeClassName="nav-link-selected" to={path || ' '}>
        {locale}{' '}
      </NavLink>
    </div>
  );
}
