import React from 'react';
import { NavLink } from 'react-router-dom';

export default function Footer() {
  return (
    <div className="section-8">
      <div className="footer-1-wrapper">
        <NavLink to="/" href="#">
          <img className="footer-logo" src={require('../../images/Disain/logo-light.svg')}></img>
        </NavLink>
        <div className="footer-1">
          <div className="footer-item item1">
            <img className="footer-icon" src={require('../../images/Disain/icons/ideal-auto-icons/time.svg')}></img>

            <div className="footer-text">
              <p>E-R 8-17 / AUTOMÜÜK 9-18, L 10-14 / PESUTÄNAV 24-7</p>
            </div>
          </div>

          <div className="footer-item item2">
            <img className="footer-icon" src={require('../../images/Disain/icons/ideal-auto-icons/location.svg')}></img>
            <div className="footer-text">
              <p>PETERBURI TEE 47/1, TALLINN 11415</p>
            </div>
          </div>

          <div className="footer-item item3">
            <img className="footer-icon" src={require('../../images/Disain/icons/ideal-auto-icons/phone.svg')}></img>
            <div className="footer-text">
              <p>+372 663 0873</p>
            </div>
          </div>

          <div className="footer-item item4">
            <img className="footer-icon" src={require('../../images/Disain/icons/ideal-auto-icons/facebook.svg')}></img>
            <div className="footer-text">
              <p>FACEBOOK</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
