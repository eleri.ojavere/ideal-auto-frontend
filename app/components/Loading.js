import React from 'react';
import { useLocation } from 'react-router-dom';
import AppDataContext from '@optimistdigital/create-frontend/universal-react/AppDataContext';

export default function Loading({ children }) {
  const { pageData } = React.useContext(AppDataContext);
  const { pathname } = useLocation();
  const pathnameFromPageData = pageData.pathname;

  if (pathname !== pathnameFromPageData)
    return (
      <div className="spinner-container">
        <div className="sk-chase">
          <div className="sk-chase-dot"></div>
          <div className="sk-chase-dot"></div>
          <div className="sk-chase-dot"></div>
          <div className="sk-chase-dot"></div>
          <div className="sk-chase-dot"></div>
          <div className="sk-chase-dot"></div>
        </div>
      </div>
    );

  return <React.Fragment>{children}</React.Fragment>;
}
