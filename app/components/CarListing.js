import React, { useState, useEffect } from 'react';
import useModal from './useModal';
import ReactDOM from 'react-dom';
import { useSpring, animated } from 'react-spring';

export default function CarsListing({ listings }) {
  const [sortType, setSortType] = useState('');
  const [isDescending, setOrderDescending] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const debouncedSearchTerm = useDebounce(searchTerm, 300);
  const { showItem, toggle } = useModal();

  let sortedListings = listings
    .sort((a, b) => {
      if (isDescending === true) {
        sortedListings = b[sortType] - a[sortType];
      } else {
        sortedListings = a[sortType] - b[sortType];
      }
      return sortedListings;
    })
    .filter((listing) => {
      return listing.model.toLowerCase().indexOf(debouncedSearchTerm.toLowerCase()) !== -1;
    });

  const handleSort = (evt) => {
    const type = evt.target.value;

    if (sortType === type) {
      setOrderDescending((old) => !old);
    } else {
      setSortType(type);
    }
  };

  const Modal = ({ hide }) =>
    showItem
      ? ReactDOM.createPortal(
          <React.Fragment>
            <div className="modal-overlay" />
            <div className="modal-wrapper" aria-modal aria-hidden tabIndex={-1} role="dialog">
              <div className="modal">
                <div className="modal-header">
                  <button
                    type="button"
                    className="modal-close-button "
                    data-dismiss="modal"
                    aria-label="Close"
                    onClick={hide}
                  >
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                {listings.name}
              </div>
            </div>
          </React.Fragment>,
          document.body
        )
      : null;

  const props = useSpring({
    to: async (next) => {
      await next({ marginTop: -100 });
      await next({ marginTop: 0 });
    },
    from: { marginTop: -100 },
  });

  return (
    <div>
      <div className="sorting">
        <div className="maxwidth-wrapper">
          <div className="sorting-wrap">
            <div className="sort-label">Sorteeri</div>

            <select onChange={handleSort}>
              <option value="id">Vaikimisi</option>
              <option value="price">Hind kasvav</option>
              <option value="price">Hind kahanev</option>
              <option value="mileage">Läbisõit kasvav</option>
              <option value="mileage">Läbisõit kahanev</option>
              <option value="year">Aasta kasvav</option>
              <option value="year">Aasta kahanev</option>
            </select>
          </div>
        </div>
      </div>
      <Modal showItem={showItem} hide={toggle} />
      <div className="listing-heading listing-row">
        <div className="maxwidth-wrapper-listings">
          <input type="text" placeholder="Otsi" onChange={(e) => setSearchTerm(e.target.value)} />
          <div className="maxwidth-wrapper">
            <div className="subheading first">Mark ja mudel</div>
            <button value="mileage" onClick={handleSort} className=" subheading">
              {sortType === 'mileage' && isDescending === true ? <svg className="desc-icon"></svg> : ''}
              {sortType === 'mileage' && isDescending === false ? <svg className="asc-icon"></svg> : ''}
              läbisõit
            </button>

            <div className="subheading">keretüüp</div>
            <div className="subheading">vedav sild</div>
            <button value="year" onClick={handleSort} className=" subheading">
              {sortType === 'year' && isDescending === true ? <svg className="desc-icon"></svg> : ''}
              {sortType === 'year' && isDescending === false ? <svg className="asc-icon"></svg> : ''}
              aasta
            </button>
            <div className="subheading">kütus</div>
            <div className="subheading">käigukast</div>
            <button value="price" onClick={handleSort} className=" subheading last">
              {sortType === 'price' && isDescending === true ? <svg className="desc-icon"></svg> : ''}
              {sortType === 'price' && isDescending === false ? <svg className="asc-icon"></svg> : ''}
              Hind
            </button>
          </div>
        </div>
      </div>

      {sortedListings.map((listing) => (
        <div key={listing.id} className="sortedlistings">
          <animated.div className="listing-body " onClick={() => toggle(listing)} style={props}>
            <div className="maxwidth-wrapper">
              <div className="image-title">
                <div className="listing-image ">
                  <img src={listing.thumbnail}></img>
                </div>

                <div className="car-image-title"> {listing.model}</div>
              </div>
              <div className="car-features">
                <div className="price-content">{listing.mileage}</div>
                <div>km</div>
              </div>
              <div className="car-features">{listing.body_type}</div>
              <div className="car-features">{listing.drivetrain}</div>
              <div className="car-features">{listing.year}</div>
              <div className="car-features">{JSON.parse(listing.fuel)}</div>
              <div className="car-features">{JSON.parse(listing.transmission)}</div>

              <div className="car-features price">
                <div className="price-content">{listing.price}</div>
                <div className="currency">EUR</div>
              </div>
            </div>
          </animated.div>
          <div className="listing-body-mobile">
            <div className="maxwidth-wrapper-mobile">
              <div className="col">
                <div className="img-wrap">
                  <img src={listing.thumbnail}></img>
                </div>
              </div>
              <div className="col-2">
                <div className="car-image-title">{listing.model}</div>
                <div className="other-info">
                  <div className="other-col">{listing.mileage} km</div>
                  <div className="other-col-2">{listing.body_type}</div>
                  <div>{listing.drivetrain}</div>
                  <div>{listing.year}</div>
                  <div>{JSON.parse(listing.fuel)}</div>
                  <div>{JSON.parse(listing.transmission)}</div>
                </div>
              </div>
              <div className="col-3">
                <div className="price-content">{listing.price} EUR</div>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
}

function useDebounce(value, delay) {
  // State and setters for debounced value
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(
    () => {
      // Update debounced value after delay
      const handler = setTimeout(() => {
        setDebouncedValue(value);
      }, delay);

      // Cancel the timeout if value changes (also on delay change or unmount)
      // This is how we prevent debounced value from updating if value is changed ...
      // .. within the delay period. Timeout gets cleared and restarted.
      return () => {
        clearTimeout(handler);
      };
    },
    [value, delay] // Only re-call effect if value or delay changes
  );

  return debouncedValue;
}
