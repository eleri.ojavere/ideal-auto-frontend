import React from 'react';
import AppDataContext from '@optimistdigital/create-frontend/universal-react/AppDataContext';

export default function CarWashSection(data) {
  return (
    <div className="section-4">
      <div className="background-opacity">
        <img className="background2-overlay" src={data.text.thumbnail}></img>
      </div>
      <div className="section-4-container">
        <div className="section-4-content">
          <div className="section-4-left">
            <div className="hexagon-wrapper">
              <div className="hexagon-top">
                <div className="hexagon-top-left">
                  <img src={require('../images/Disain/honeycombs/green_honeycomb.svg')}></img>

                  <div className="hexagon-text">Ligipääs 24/7</div>
                </div>

                <div className="hexagon-top-right">
                  <img src={require('../images/Disain/honeycombs/black_honeycomb.svg')}></img>

                  <div className="hexagon-text">kärcheri kvaliteet</div>
                </div>
              </div>

              <div className="hexagon-bottom">
                <img src={require('../images/Disain/honeycombs/white_honeycomb.svg')}></img>

                <div className="hexagon-text">Numbri-tuvastus</div>
              </div>
            </div>
          </div>
          <div className="section-4-right">
            <div className="title1">
              {data.text.title}
              <span className="span-bold">
                {' '}
                <span className="green-text"> </span>
              </span>
            </div>
            <div className="line2"></div>
            <div className="title2">{data.text.description}</div>
            <div className="small-device-content">
              {data.text.honeycomb_titles.map((item, i) => (
                <div className="services-wrap" key={i}>
                  <img
                    className="checkmark"
                    src={require('../images/Disain/icons/ideal-auto-icons/checkmark_full.svg')}
                  ></img>
                  <span> {item.attributes.title}</span>
                </div>
              ))}
            </div>
            <button className="btn-border">
              <span className="btn-text">{data.text.button_1}</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
