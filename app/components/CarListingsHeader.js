import React from 'react';

export default function CarsListingHeader() {
  return (
    <div className="listing-heading listing-row">
      <div className="maxwidth-wrapper">
        <div className="subheading first">Mark ja mudel</div>
        <div className="subheading">läbisõit</div>
        <div className="subheading">keretüüp</div>
        <div className="subheading">vedav sild</div>
        <div className="subheading">aasta</div>
        <div className="subheading">kütus</div>
        <div className="subheading">käigukast</div>
        <div className="subheading last">hind</div>
      </div>
    </div>
  );
}
