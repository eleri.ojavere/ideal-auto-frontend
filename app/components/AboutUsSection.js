import React from 'react';
import AppDataContext from '@optimistdigital/create-frontend/universal-react/AppDataContext';

export default function AboutUsSection(data) {
  return (
    <div className="section-6">
      <div className="section-container">
        <div className="title1">
          {data.text.title}
          <div className="underline"></div>
        </div>

        <div className="diamonds-wrap">
          <div className="diamond-wrap-2">
            <div className="diamond"></div>
          </div>
          <div className="diamond-wrap-2">
            <div className="diamond"></div>
          </div>
          <div className="diamond-wrap-2">
            <div className="diamond"></div>
          </div>
        </div>

        <div className="title2">{data.text.description}</div>

        <button className="btn-border">
          <span className="btn-text">{data.text.button_1}</span>
        </button>
      </div>
    </div>
  );
}
