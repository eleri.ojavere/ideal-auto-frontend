import React from 'react';
import AppDataContext from '@optimistdigital/create-frontend/universal-react/AppDataContext';

export default function CarWashGallerySection(data) {
  return (
    <div className="section-5">
      <div className="images-container">
        <div className="left-container">
          <div className="image">
            <img className="left-image" src={data.text.thumbnail}></img>
          </div>
        </div>

        <div className="right-container">
          {data.text.right_side_images.map((item, i) => (
            <div className="image" key={i}>
              <img className="image1" src={item.attributes.thumbnail}></img>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
