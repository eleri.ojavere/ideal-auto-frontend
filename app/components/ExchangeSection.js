import React from 'react';
import AppDataContext from '@optimistdigital/create-frontend/universal-react/AppDataContext';

export default function ExchangeSection(data) {
  const totalWords = data.text.title;

  const firstWord = totalWords.replace(/ .*/, '');

  const otherWords = totalWords.split(' ').slice(1, 3).join(' ');

  return (
    <div className="section-2">
      <div className="section-2-left">
        <img className="honeycomb-section-2" src={require('../images/Disain/honeycombs/honeycomb_MERGED.svg')}></img>
      </div>
      <div className="section-2-right">
        <div className="title1">
          {firstWord}
          <span className="green-text new-line"> {otherWords}</span>
        </div>
        <div className="line2"></div>
        <div className="title2">{data.text.description}</div>
        <div className="section-2-third">
          {data.text.promises.map((item, i) => (
            <div className="title3-wrap" key={i}>
              <div className="diamond-wrap">
                <div className="diamond"></div>
              </div>
              <div className="title3">{item.attributes.promise}</div>
            </div>
          ))}
        </div>

        <div className="button-wrapper">
          <button className="btn-secondary">
            <span className="btn-text">{data.text.button_1}</span>
          </button>

          <button className="btn-border">
            <span className="btn-text">{data.text.button_2}</span>
          </button>
        </div>
      </div>
    </div>
  );
}
