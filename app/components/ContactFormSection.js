import TextareaAutosize from 'react-textarea-autosize';
import React, { useState, useForm } from 'react';
import sendMail from '../api/sendMail';

export default function ContactFormSection({ formInfo = '' }) {
  const [errorMessages, setErrors] = useState(null);

  const [isSent, setIsSent] = useState(false);

  async function sendEmail(event) {
    event.preventDefault();
    event.persist();
    const data = {
      name: event?.target?.name?.value ?? null,
      email: event?.target?.email?.value ?? null,
      phone: event?.target?.phone?.value ?? null,
      content: event?.target?.content?.value ?? null,
      extra: formInfo,
    };
    const response = await sendMail('contact-us', data);
    if (response.errors) {
      setIsSent(false);
      setErrors(Object.values(response.errors));
      return false;
    }
    setErrors(null);
    setIsSent(true);
    event.target.reset();
    return false;
  }

  return (
    <div className="contact-form-section">
      <div className="contact-form-wrapper">
        <div className="section-content">
          <div className="contact-form-title">Kirjuta meile</div>
          <form id="contact-form" onSubmit={sendEmail}>
            <div className="form-inputs">
              <div className="form-inputs-left">
                <div className="form-input">
                  <FormInputField type={'text'} name={'name'} label={'Nimi'} />
                </div>
                <div className="form-input">
                  <FormInputField type={'text'} name={'phone'} label={'Telefon'} />
                </div>
                <div className="form-input">
                  <FormInputField type={'email'} name={'email'} label={'Email'} />
                </div>
              </div>
              <div className="form-inputs-right">
                <div className="form-input">
                  <FormTextAreaField type={'text'} name={'content'} label={'Sõnum'} />
                </div>

                {<div className="success-message"> {isSent ? 'Sõnum saadetud!' : ''}</div>}

                {errorMessages && (
                  <div className={'error-message'}>
                    {Object.values(errorMessages).map((error, index) => {
                      return <p key={index}>{error}</p>;
                    })}
                  </div>
                )}

                <div className="btn-secondary-form">
                  <input type="submit" value="saada" />
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

function FormInputField({ type, name, label }) {
  return (
    <div className={'form-field'}>
      <label className={'form-label'}>{label}</label>
      <input type={type} name={name} />
    </div>
  );
}

function FormTextAreaField({ label, name }) {
  return (
    <div className={'form-field'}>
      <label className={'form-label'}>{label}</label>
      <TextareaAutosize name={name}></TextareaAutosize>
    </div>
  );
}
