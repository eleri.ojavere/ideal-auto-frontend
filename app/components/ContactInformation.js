import React from 'react';

export default function ContactInformation() {
  return (
    <div className="contactinfo-section-container">
      <div className="image-wrapper">
        <div className="image-wrap"></div>
      </div>
      <div className="section-wrapper">
        <div className="section-content">
          <div className="title">kontakt</div>

          <div className="line2"></div>
          <div className="contact-info-wrapper">
            <div className="contact-info-item">
              <img src={require('../images/Disain/icons/ideal-auto-icons/contact_page_icons/check.svg')}></img>
              <div className="contact-info-text">
                <div className="heading">Teenindus:</div>
                <div className="value">E-R 08:00-19:00</div>
              </div>
            </div>
            <div className="contact-info-item">
              <div className="cars-svg"></div>
              <div className="contact-info-text">
                <div className="heading">Kasutatud autode müük:</div>
                <div className="value">E-R 10:00-17:00, L kokkuleppel</div>
              </div>
            </div>
            <div className="contact-info-item">
              <div className="cars-shower-svg"></div>

              <div className="contact-info-text">
                <div className="heading">Pesutänav:</div>
                <div className="value">24/7</div>
              </div>
            </div>
            <div className="contact-info-item">
              <img src={require('../images/Disain/icons/ideal-auto-icons/contact_page_icons/location.svg')}></img>
              <div className="contact-info-text">
                <div className="heading">Aadress:</div>
                <div className="value">Peterburi tee 47C, Tallinn 11415</div>
              </div>
            </div>
            <div className="contact-info-item">
              <img src={require('../images/Disain/icons/ideal-auto-icons/contact_page_icons/call.svg')}></img>
              <div className="contact-info-text">
                <div className="heading">Telefon:</div>
                <div className="value">+372 663 0873</div>
              </div>
            </div>
            <div className="contact-info-item">
              <img src={require('../images/Disain/icons/ideal-auto-icons/contact_page_icons/email.svg')}></img>
              <div className="contact-info-text">
                <div className="heading">Email:</div>
                <div className="value">info@idealauto.ee</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
