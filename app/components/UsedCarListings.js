import React, { useState, useForm } from 'react';
import fetchListings from '../api/fetchListings';
import Loading from './Loading';
import CarListing from './CarListing';

export default function UsedCarListings() {
  const [listings, setListings] = useState(null);

  React.useEffect(() => {
    (async function () {
      setListings((await fetchListings()).listings); // Laeme APIst listingud
    })();
  }, []);

  if (listings === null) return <Loading />;
  return <CarListing listings={listings} />;
}
