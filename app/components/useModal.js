import React, { useState } from 'react';
import fetchListings from '../api/fetchListings';

const useModal = () => {
  const [showItem, setItemShowing] = useState(false);
  const [listings, setListings] = useState(null);

  React.useEffect(() => {
    (async function () {
      setListings((await fetchListings()).listings); // Laeme APIst listingud
    })();
  }, []);

  function toggle() {
    setItemShowing(!showItem);
    setListings(!showItem);
    console.log(!showItem);
  }

  return {
    showItem,
    toggle,
  };
};

export default useModal;
