import React from 'react';
import AppDataContext from '@optimistdigital/create-frontend/universal-react/AppDataContext';

export default function GoogleMapsSection(data) {
  return (
    <div className="section-7">
      <div className="gmap-wrap">
        <div>
          <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2029.3580240311!2d24.81049935195214!3d59.42710318160325!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4692edb048e7b981%3A0x3b9885efcffc4303!2sIdeal%20Auto!5e0!3m2!1sen!2see!4v1584432898527!5m2!1sen!2see"
            width="600"
            height="450"
            frameBorder="0"
          ></iframe>
        </div>
      </div>
      <div className="office">
        <img className="office-photo" src={data.text.thumbnail}></img>
      </div>
    </div>
  );
}
