import fetch from 'node-fetch';

export default async function fetchContent(slug, params) {
  const content = await fetch(
    `http://ideal-auto-cms.test/api/content/?slug=${slug}${params ? '&' + params.substring(1) : ''}`,
    {
      method: 'GET',
      mode: 'cors',
      headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
    }
  ).then((res) => {
    if (!res.ok) throw res;
    if (res.headers.get('content-type').includes('application/json')) return res.json();
    return res.text();
  });
  if (content.exception) throw content;
  if (content.to_url && content.from_url) return { redirect: content };
  return {
    content: content.content,
    locales: content.structure.locales,
  };
}
