import fetch from 'node-fetch';

export default async function sendMail(slug, data) {
  return await fetch('http://ideal-auto-cms.test/api/contact-us', {
    method: 'POST',
    mode: 'cors',
    headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
    body: JSON.stringify(data),
  })
    .then((res) => res.json())
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.log('ERROR', error);
    });
}
