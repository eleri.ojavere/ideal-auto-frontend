import fetch from 'node-fetch';

export default async function fetchListings(id) {
  const listings = await fetch(`http://ideal-auto-cms.test/api/listings`, {
    method: 'GET',
    mode: 'cors',
    headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
  }).then((res) => {
    if (!res.ok) throw res;
    if (res.headers.get('content-type').includes('application/json')) return res.json();
    return res.text();
  });

  if (listings.exception) throw listings;
  if (listings.to_url && listings.from_url) return { redirect: listings };
  console.log(listings);
  return {
    listings: listings,
  };
}
