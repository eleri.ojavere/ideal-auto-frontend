import React from 'react';
import { Helmet } from 'react-helmet-async';
import Header from '../components/sections/Header';
import Footer from '../components/sections/Footer';
import AppDataContext from '@optimistdigital/create-frontend/universal-react/AppDataContext';
import UsedCarsSaleInfo from '../components/UsedCarsSaleInfo';
import UsedCarListings from '../components/UsedCarListings';
import CarListingsHeader from '../components/CarListingsHeader';
import Loading from '../components/Loading';

export default function CarsSalePage() {
  const { content } = React.useContext(AppDataContext).pageData;

  const data = content?.data?.content ?? null;

  return (
    <React.Fragment>
      <Loading>
        <Helmet>
          <title>Ideal Auto - {content?.name}</title>
        </Helmet>
        <Header />
        <CarsSalePageContent data={data} />
        <UsedCarListings />
        <Footer />
      </Loading>
    </React.Fragment>
  );
}

function CarsSalePageContent({ data }) {
  const contentTypes = (attributes, layout, index) => {
    switch (layout) {
      case 'description':
        return <UsedCarsSaleInfo text={attributes} key={index} />;
      default:
        return null;
    }
  };

  return Object.values(data).map((object, index) => {
    const component = contentTypes(object?.attributes, object?.layout, index) ?? null;
    return component;
  });
}

// CarsSalePage.getPageData = async (location) => {
//   const listings = await fetchListings(location.pathname, location.search);

//   return (prevState) => ({
//     ...prevState,
//     listings: listings,
//   });
// };
