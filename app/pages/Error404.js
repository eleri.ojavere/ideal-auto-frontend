import React from 'react';
import { Helmet } from 'react-helmet-async';
import Header from '../components/sections/Header';
import Footer from '../components/sections/Footer';
import AppDataContext from '@optimistdigital/create-frontend/universal-react/AppDataContext';
import Loading from '../components/Loading';

export default function Error404() {
  const appData = React.useContext(AppDataContext);
  if (appData.serverContext) appData.serverContext.status = 404;

  return (
    <React.Fragment>
      <Helmet>
        <title>Not found</title>
      </Helmet>
      <Header />
      <Loading>
        <div className="error-page-container">
          <div className="error-page">
            <div className="error-code">404</div>
            <div className="error-text">Page not found</div>
          </div>
        </div>

        <Footer />
      </Loading>
    </React.Fragment>
  );
}
