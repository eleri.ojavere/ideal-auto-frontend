import React from 'react';
import { Helmet } from 'react-helmet-async';
import Header from '../components/sections/Header';
import Footer from '../components/sections/Footer';
import GoogleMapsSection from '../components/GoogleMapsSection';
import AppDataContext from '@optimistdigital/create-frontend/universal-react/AppDataContext';
import ContactInformation from '../components/ContactInformation';
import ContactFormSection from '../components/ContactFormSection';
import Loading from '../components/Loading';

export default function ContactPage() {
  const { content } = React.useContext(AppDataContext).pageData;

  const data = content?.data?.content ?? null;

  return (
    <React.Fragment>
      <Loading>
        <Helmet>
          <title>Ideal Auto - {content?.name}</title>
        </Helmet>
        <Header />
        <ContactInformation />
        <ContactFormSection />
        <ContactPageContent data={data} />
        <Footer />
      </Loading>
    </React.Fragment>
  );
}

function ContactPageContent({ data }) {
  const contentTypes = (attributes, layout, index) => {
    switch (layout) {
      case 'google-maps':
        return <GoogleMapsSection text={attributes} key={index} />;
      default:
        return null;
    }
  };

  return Object.values(data).map((object, index) => {
    const component = contentTypes(object?.attributes, object?.layout, index) ?? null;
    return component;
  });
}
