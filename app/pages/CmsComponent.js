import HomePage from './HomePage';
import ContactPage from './ContactPage';
import Error404 from './Error404';
import CarsSalePage from './CarsSalePage';
import React from 'react';
import { AppDataContext } from '@optimistdigital/create-frontend/universal-react';

export default function CmsComponent() {
  const templates = {
    'home-page': HomePage,
    'cars-sale': CarsSalePage,
    'contact-page': ContactPage,
  };
  const { content } = React.useContext(AppDataContext).pageData;

  const getComponent = (page) => templates[page] ?? null;
  const Component = getComponent(content?.template);
  return Component ? <Component /> : <Error404 />;
}
