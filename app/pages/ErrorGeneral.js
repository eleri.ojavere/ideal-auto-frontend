import React from 'react';
import { AppDataContext } from '@optimistdigital/create-frontend/universal-react';
import Loading from '../components/Loading';

export default function ErrorGeneral({ status }) {
  const appData = React.useContext(AppDataContext);
  if (appData.serverContext) appData.serverContext.status = status;

  return (
    <main>
      <Loading>
        <div className="error-page-container">
          <div className="error-page">
            <div className="error-code">{status}</div>
            <div className="error-text">Server error</div>
          </div>
        </div>
      </Loading>
    </main>
  );
}
