import React from 'react';
import { Helmet } from 'react-helmet-async';
import Header from '../components/sections/Header';
import Footer from '../components/sections/Footer';
import FirstSection from '../components/FirstSection';
import ExchangeSection from '../components/ExchangeSection';
import ClaimsSection from '../components/ClaimsSection';
import CarWashGallerySection from '../components/CarWashGallerySection';
import CarWashSection from '../components/CarWashSection';
import AboutUsSection from '../components/AboutUsSection';
import GoogleMapsSection from '../components/GoogleMapsSection';
import AppDataContext from '@optimistdigital/create-frontend/universal-react/AppDataContext';
import Loading from '../components/Loading';
import { useLocation } from 'react-router-dom';

export default function HomePage() {
  const { content } = React.useContext(AppDataContext).pageData;

  const data = content?.data?.content ?? null;

  return (
    <React.Fragment>
      <Loading>
        <Helmet>
          <title>Ideal Auto - {content?.name}</title>
        </Helmet>

        <Header />
        <HomePageContent data={data} />
        <Footer />
      </Loading>
    </React.Fragment>
  );
}

function HomePageContent({ data }) {
  const contentTypes = (attributes, layout, index) => {
    switch (layout) {
      case 'services':
        return <FirstSection text={attributes} key={index} />;
      case 'exchange':
        return <ExchangeSection text={attributes} key={index} />;
      case 'claims':
        return <ClaimsSection text={attributes} key={index} />;
      case 'carwash':
        return <CarWashSection text={attributes} key={index} />;
      case 'carwash-gallery':
        return <CarWashGallerySection text={attributes} key={index} />;
      case 'about-us':
        return <AboutUsSection text={attributes} key={index} />;
      case 'google-maps':
        return <GoogleMapsSection text={attributes} key={index} />;
      default:
        return null;
    }
  };

  return Object.values(data).map((object, index) => {
    const component = contentTypes(object?.attributes, object?.layout, index) ?? null;
    return component;
  });
}
