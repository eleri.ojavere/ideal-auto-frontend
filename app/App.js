import 'app/scss/entry.scss';
import { Helmet } from 'react-helmet-async';
import { Switch, Route } from 'react-router-dom';
import ErrorBoundary from 'app/components/ErrorBoundary';
import ErrorGeneral from 'app/pages/ErrorGeneral';
import React from 'react';
import Router, { getRouteData } from '@optimistdigital/create-frontend/universal-react/Router';
import Routes from 'app/routes';
import fetchContent from './api/fetchContent';

export default function App() {
  return (
    <React.Fragment>
      <Helmet>
        <title>New app</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Helmet>

      <ErrorBoundary renderError={() => <ErrorGeneral status={500} />}>
        <Router>
          <Routes />
        </Router>
      </ErrorBoundary>
    </React.Fragment>
  );
}

/**
 * This function gets called once in the server, and in the client whenever the page changes.
 * The result ends up in the AppDataContext.
 */
App.getPageData = async (location, props) => {
  try {
    const content = await fetchContent(location.pathname, location.search);

    return (prevState) => ({
      // Merge in the data from the route components
      ...prevState,
      // You can set data here that will be added on every page
      config: prevState.config || props.config,
      locales: content?.locales ?? null,
      content: content?.content,
      pathname: location.pathname,
      activeLocale: content?.locales?.active ?? null,
      statusCode: 200,
    });
  } catch (err) {
    console.error('Caught error while fetching data on server:', err);
    return (prevState) => ({
      config: prevState.config || props.config,
      statusCode: (err.response ? err.response.status : err.status) ?? 500,
    });
  }
};
