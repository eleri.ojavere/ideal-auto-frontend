import React from 'react';
import { Switch, Route } from 'react-router-dom';
import CmsComponent from './pages/CmsComponent';
import Error404 from './pages/Error404';

export default function Routes() {
  return (
    <Switch>
      <Route exact path="/error" component={Error404} />
      <Route component={CmsComponent} />
    </Switch>
  );
}
